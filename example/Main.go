package main

import (
	"fmt"
	"gitee.com/jjawesomejj/awesomehttp/boot"
	"gitee.com/jjawesomejj/awesomehttp/example/handler"
	"gitee.com/jjawesomejj/awesomehttp/example/middlewares"
	awesomehttp "gitee.com/jjawesomejj/awesomehttp/system"
	"gitee.com/jjawesomejj/awesomehttp/system/httpContext"
	"os"
	"strconv"
	"time"
)

type testStruct struct {
	name string
}

func main() {
	port := 80
	resourcePath := "web"
	if len(os.Args) >= 2 {
		newPort, err := strconv.Atoi(os.Args[1])
		if err != nil {
			panic("不是一个合法的端口:" + os.Args[1])
		}
		port = newPort
	}
	if len(os.Args) >= 3 {
		resourcePath = os.Args[2]
	}
	fmt.Println("静态资源路径", resourcePath)
	application := boot.DefaultApp()
	application.Port = float64(port)                               //监听端口
	application.IpAddress = "0.0.0.0"                              //监听ip地址
	application.ResourcePath = resourcePath                        //静态资源路径
	application.SetGlobalMiddleware(&middlewares.JsonMiddleware{}) //设置全局访问中间件
	//注册模块
	application.RegisterModule(func() awesomehttp.HttpHandler {
		return &handler.ExampleHandler{}
	}) //模块化注册 此结构体内部定义的全部以Action函数为前置的路由将映射为一个接口
	//注册普通路由
	err := application.AddRoute(awesomehttp.Route{
		Router:        "/api/project/add", //正则路由
		RequestMethod: awesomehttp.REQUEST_METHOD_POST,
		RunFun: func(ctx *httpContext.HttpContext) interface{} {
			return map[string]interface{}{
				"postData":     ctx.PostData,
				"routerParams": ctx.RouteParams, //路由参数
			}
		},
		//RouterReg: map[string]string{
		//	"name": "^j(.*?)h(.*?)q",
		//	"id":   "\\d+",
		//}, //路由正则过滤 可不配置
		//Middlewares: []awesomehttp.Middleware{&middlewares2.BaseMiddleware{}}, //单个路由中间件
	},
	)
	//application.Group([]awesomehttp.Middleware{&middlewares2.BaseMiddleware{}}, "cms/", func() {
	//	application.AddRoute(awesomehttp.Route{
	//		Router: "login",
	//		RunFun: func(ctx *httpContext.HttpContext) interface{} {
	//			return ctx.GetMany()
	//		},
	//		RequestMethod: awesomehttp.REQUEST_METHOD_GET,
	//	})
	//}) //路由组 url 前缀
	if err != nil {
		fmt.Println(err)
		return
	}
	go application.Listen()
	for true {
		time.Sleep(time.Second * 30)
	}
}
