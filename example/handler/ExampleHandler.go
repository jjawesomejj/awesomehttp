package handler

import awesomehttp "gitee.com/jjawesomejj/awesomehttp/system"

type ExampleHandler struct {
	awesomehttp.HttpBaseHandler
}

func (handler *ExampleHandler) GetModuleName() string {
	return "/"
}
func (handler *ExampleHandler) ActionIndex() interface{} {
	return map[string]interface{}{}
}
