
### awesome http 组件
### 提供路由,正则路由(参照laravel设计),资源路由(参照yii设计),分组路由,中间件 使用方式参照example

#### example
```shell
    port := 8083
	resourcePath := "web"
	if len(os.Args) >= 2 {
		newPort, err := strconv.Atoi(os.Args[1])
		if err != nil {
			panic("不是一个合法的端口:" + os.Args[1])
		}
		port = newPort
	}
	if len(os.Args) >= 3 {
		resourcePath = os.Args[2]
	}
	fmt.Println("静态资源路径", resourcePath)
	application := boot.DefaultApp()
	application.Port = float64(port)                               //监听端口
	application.IpAddress = "0.0.0.0"                              //监听ip地址
	application.ResourcePath = resourcePath                        //静态资源路径
	application.SetGlobalMiddleware(&middlewares.JsonMiddleware{}) //设置全局访问中间件
	//注册模块
	application.RegisterModule(func() awesomehttp.HttpHandler {
		return &handler.ExampleHandler{}
	}) //模块化注册 此结构体内部定义的全部以Action函数为前置的路由将映射为一个接口
	//注册普通路由
	err := application.AddRoute(awesomehttp.Route{
		Router:        "/test/{name}/{id}", //正则路由
		RequestMethod: awesomehttp.REQUEST_METHOD_GET,
		RunFun: func(ctx *httpContext.HttpContext) interface{} {
			return map[string]interface{}{
				"routerParams": ctx.RouteParams, //路由参数
			}
		},
		RouterReg: map[string]string{
			"name": "^j(.*?)h(.*?)q",
			"id":   "\\d+",
		}, //路由正则过滤 可不配置
		Middlewares: []awesomehttp.Middleware{&middlewares2.BaseMiddleware{}}, //单个路由中间件
	},
	)
	application.Group([]awesomehttp.Middleware{&middlewares2.BaseMiddleware{}}, "cms/", func() {
		application.AddRoute(awesomehttp.Route{
			Router: "login",
			RunFun: func(ctx *httpContext.HttpContext) interface{} {
				return ctx.GetMany()
			},
			RequestMethod: awesomehttp.REQUEST_METHOD_GET,
		})
	}) //路由组 url 前缀
	if err != nil {
		return
	}
	go application.Listen()
	for true {
		time.Sleep(time.Second * 30)
	}
```