package awesomehttp

import (
	"reflect"
)

type ReflectTool struct {

}

func (reflectTool *ReflectTool) GetAllMethods(obj interface{})[]string  {
	methods:=make([]string,0)
	reflectObj :=reflect.TypeOf(obj)
	for i:=0;i< reflectObj.NumMethod();i++ {
		methods=append(methods,reflectObj.Method(i).Name)
	}
	return methods
}
func (reflectTool *ReflectTool)CallFunByName(obj interface{}, fun string, params ...interface{}) interface{} {
	inputs := make([]reflect.Value, len(params))
	for k, pamra := range params {
		inputs[k] = reflect.ValueOf(pamra)
	}
	result := reflect.ValueOf(obj).MethodByName(fun).Call(inputs)
	return result
}

func (reflectTool *ReflectTool) MethodExist(obj interface{}, fun string) bool {
	return reflect.ValueOf(obj).MethodByName(fun).IsValid()
}
