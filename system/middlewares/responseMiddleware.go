package middlewares

import (
	"gitee.com/jjawesomejj/awesomehttp/system/helper"
	"gitee.com/jjawesomejj/awesomehttp/system/httpContext"
)

type BaseMiddleware struct {
}
type ResponseMiddleware struct {
}

func (baseMiddleware *BaseMiddleware) Handle(ctx *httpContext.HttpContext, next func(ctx2 *httpContext.HttpContext) interface{}) (res interface{}) {
	return next(ctx)
}
func (middleware *ResponseMiddleware) WriteResponse(ctx *httpContext.HttpContext) {
	responseValue := ctx.ResponseContent
	w := ctx.Response
	switch responseValue.(type) {
	case string:
		w.Write([]byte(responseValue.(string)))
		break
	case []byte:
		w.Write(responseValue.([]byte))
		break
	case map[string]interface{}:
		w.Write([]byte(helper.JsonEncode(responseValue)))
		break
	default:
		w.Write([]byte(helper.JsonEncode(responseValue)))
		break
	}
}
func (middleware *ResponseMiddleware) Handle(ctx *httpContext.HttpContext, next func(ctx2 *httpContext.HttpContext) interface{}) (res interface{}) {
	data := next(ctx)
	middleware.WriteResponse(ctx)
	return data
}
