package awesomehttp

import "gitee.com/jjawesomejj/awesomehttp/system/httpContext"

type Middleware interface {
	Handle(ctx *httpContext.HttpContext, next func(ctx2 *httpContext.HttpContext) interface{}) (res interface{})
}
