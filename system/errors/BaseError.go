package errors

type BaseError struct {
	Code           int
	HttpStatusCode int
	Message        string
}

func (baseError *BaseError) Error() string {
	return baseError.Message
}
