package awesomehttp

import (
	"gitee.com/jjawesomejj/awesomehttp/system/httpContext"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

/**
通用模块名称
*/
type HttpHandler interface {
	GetModuleName() string
	SetHttpInfo(ctx *httpContext.HttpContext) HttpHandler
	InitRequest() HttpHandler
}

type HttpBaseHandler struct {
	Response    http.ResponseWriter
	Request     *http.Request
	GetData     map[string]interface{}
	PostData    map[string]interface{}
	StartAt     int64
	HttpContext *httpContext.HttpContext
}

func (handler *HttpBaseHandler) GetModuleName() string {
	return "/"
}
func (handler *HttpBaseHandler) SetHttpInfo(ctx *httpContext.HttpContext) HttpHandler {
	handler.Request = ctx.Request
	handler.Response = ctx.Response
	handler.HttpContext = ctx
	return handler
}
func (handler *HttpBaseHandler) InitRequest() HttpHandler {
	handler.recvPostData()
	handler.recvGetData()
	return handler
}
func (handler *HttpBaseHandler) recvGetData() *HttpBaseHandler {
	if handler.GetData == nil {
		getData := make(map[string]interface{})
		queryString := strings.Split(handler.Request.RequestURI, "?")
		if len(queryString) < 2 {
			handler.GetData = map[string]interface{}{}
		} else {
			getData = parseParamsStrings(queryString[1])
		}

		handler.GetData = getData
	}
	return handler
}
func parseParamsStrings(queryParamsString string) map[string]interface{} {
	res := make(map[string]interface{})
	queryParamsList := strings.Split(queryParamsString, "&")
	for _, value := range queryParamsList {
		paramsInfo := strings.Split(value, "=")
		if len(paramsInfo) == 2 {
			res[paramsInfo[0]] = paramsInfo[1]
		} else {
			res[paramsInfo[0]] = nil
		}
	}
	return res
}
func (handler *HttpBaseHandler) recvPostData() *HttpBaseHandler {
	if handler.PostData == nil {
		handler.PostData = make(map[string]interface{})
		a1, _ := ioutil.ReadAll(handler.Request.Body)
		parmas := parseParamsStrings(string(a1))
		for key, value := range parmas {
			if value != nil {
				postValue, _ := url.QueryUnescape(value.(string))
				handler.PostData[key] = postValue
			}
		}
	}
	return handler
}
func (handler *HttpBaseHandler) GetMany(keys ...string) map[string]interface{} {
	handler.recvGetData()
	res := make(map[string]interface{})
	if len(keys) != 0 {
		for _, key := range keys {
			res[key] = handler.GetData[key]
		}
	} else {
		res = handler.GetData
	}
	return res
}
func (handler *HttpBaseHandler) Get(key string) interface{} {
	return handler.Request.URL.Query().Get(key)
}
func (handler *HttpBaseHandler) PostMany(keys ...string) map[string]interface{} {
	res := make(map[string]interface{})
	if len(keys) == 0 {
		res = handler.PostData
	} else {
		for _, key := range keys {
			res[key] = handler.PostData[key]
		}
	}
	return res
}
func (handler *HttpBaseHandler) PostOne(key string, defalut ...interface{}) interface{} {
	if value, ok := handler.PostData[key]; ok {
		return value
	}
	if len(defalut) > 0 {
		return defalut[0]
	} else {
		return nil
	}
}

func (handler *HttpBaseHandler) SetCookie(name string, value string, path string, expire int, domain string) *HttpBaseHandler {
	if domain == "" {
		domain = handler.Request.Host
	}
	cookie := &http.Cookie{
		Name:   name,   //名字
		Value:  value,  //值
		Path:   path,   //路径
		Domain: domain, //域名
		MaxAge: expire, //存活时间
	}
	http.SetCookie(handler.Response, cookie)
	return handler
}
