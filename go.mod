module gitee.com/jjawesomejj/awesomehttp

replace gitee.com/jjawesomejj/awesomehttp => ../awesomehttp

go 1.16

require github.com/gorilla/websocket v1.4.2 // indirect
