package boot

import (
	"fmt"
	awesomehttp "gitee.com/jjawesomejj/awesomehttp/system"
	"gitee.com/jjawesomejj/awesomehttp/system/helper"
	"gitee.com/jjawesomejj/awesomehttp/system/middlewares"
	"net/http"
)

func DefaultApp() *awesomehttp.HttpServerObj {
	server := awesomehttp.HttpServerObj{
		Port:         80,
		IpAddress:    "0.0.0.0",
		ResourcePath: "web",
	}
	if server.ErrAction == nil {
		server.ErrAction = func(w http.ResponseWriter, r *http.Request, err error) {
			fmt.Println(helper.PrintStackTrace(err))
			w.WriteHeader(500)
			w.Header().Add("Access-Control-Allow-Origin", "*") //允许访问所有域
			w.Write([]byte(helper.JsonEncode(map[string]interface{}{
				"code": 500,
				"msg":  "awesomeTask 无法处理此请求",
			})))
		}
	}
	if server.NotFindAction == nil {
		server.NotFindAction = func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(404)
			w.Write([]byte(helper.JsonEncode(map[string]interface{}{
				"code": 404,
				"msg":  "无法处理此请求",
			})))
		}
	}
	server.GlobalMiddleware = []awesomehttp.Middleware{&middlewares.ResponseMiddleware{}}
	return &server
}
